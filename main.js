const { app, BrowserWindow, session, Menu, nativeImage } = require('electron');
const fs = require('fs');
const path = require('path');
const AutoLaunch = require('auto-launch');

const appInstallDir = path.dirname(app.getAppPath());
const cookieFilePath = path.join(appInstallDir, 'cookies.json');


let mainWindow;
//hide windows menu
Menu.setApplicationMenu(null);

app.on('ready', () => {

  //add to autostart program
  let autoLaunch = new AutoLaunch({
      name: 'MyTrax',
      path: app.getPath('exe'),
    });
    autoLaunch.isEnabled().then((isEnabled) => {
      if (!isEnabled) autoLaunch.enable();
    });

  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
    icon: nativeImage.createFromPath(path.join(__dirname, 'mytrax.png')),
    webPreferences: {
      nodeIntegration: true,
    },
  });

  mainWindow.loadURL('https://ampache-pharma.arthur.com.my');

  mainWindow.webContents.on('did-finish-load', async () => {
    try {
      // Check if cookie file exists
      // this indicate the device has logged in before and have saved cookies
      const cookieFileExists = fs.existsSync(cookieFilePath);

      if (cookieFileExists) {
        // Read cookies from the file
        const cookieData = fs.readFileSync(cookieFilePath, 'utf8');
        const cookiesFromFile = JSON.parse(cookieData);

        // Check if the DEVICE_LOCK cookie exists
        const deviceLockCookie = cookiesFromFile.find(cookie => cookie.name === 'DEVICE_LOCK');

        if (deviceLockCookie) {
        // Set cookies in the session
          cookiesFromFile.forEach(async (cookie) => {
              if (!cookie.url) {
                // Set the URL for the cookie from the file
                cookie.url = 'https://ampache-pharma.arthur.com.my';
              }
              try {
                await session.defaultSession.cookies.set(cookie);
                console.log(`[READ} Cookie ${cookie.name} set from file.`);
              } catch (error) {
                console.error(`Error setting cookie ${cookie.name} from file:`, error);
              }
          });
        }
      } else {
        // Retrieve cookies to be saved into the device
        const cookies = await session.defaultSession.cookies.get({ url: 'https://ampache-pharma.arthur.com.my/mytrax_setup.php' });

        const updatedCookiesPromises = cookies.map((cookie) => {
          if (['DEVICE_LOCK', 'ampache_user'].includes(cookie.name)) {
            // Extend expiration for specific cookies
            const updatedCookie = {
              url: 'https://ampache-pharma.arthur.com.my', // Set the correct URL
              name: cookie.name,
              value: cookie.value,
              domain: cookie.domain,
              path: cookie.path,
              expirationDate: Math.floor((Date.now() / 1000) + (10 * 365 * 24 * 60 * 60)),
              secure: cookie.secure,
              httpOnly: cookie.httpOnly,
              sameSite: cookie.sameSite,
            };

          // Update the cookie with extended expiration
          return session.defaultSession.cookies.set(updatedCookie);
          }
          return Promise.resolve(); // For cookies not requiring an update
        });

        await Promise.all(updatedCookiesPromises);

        // Save updated cookies to a file
        fs.writeFileSync(cookieFilePath, JSON.stringify(cookies));
        
        console.log('[WRITE] Cookies updated and saved to file!');
      }
    } catch (error) {
      console.error('Error handling cookies:', error);
    }
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
});
